package com.devcamp.person.model;

public class Worker extends Person {
    private int salary;

    public Worker(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }

    public Worker(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void eat() {
        System.out.println("Worker eating...");
    }

    public String working() {
        return new String("Worker is working...");
    }
}
