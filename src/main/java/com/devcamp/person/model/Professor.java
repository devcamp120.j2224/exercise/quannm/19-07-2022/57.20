package com.devcamp.person.model;

public class Professor extends Person {
    private int salary;

    public Professor(int age, String gender, String name, Address address) {
        super(age, gender, name, address);
    }

    public Professor(int age, String gender, String name, Address address, int salary) {
        super(age, gender, name, address);
        this.salary = salary;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    @Override
    public void eat() {
        System.out.println("Professor eating...");
    }

    public String teaching() {
        return new String("Professor teaching...");
    }
}
