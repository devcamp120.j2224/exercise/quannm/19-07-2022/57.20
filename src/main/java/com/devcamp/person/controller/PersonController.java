package com.devcamp.person.controller;

import com.devcamp.person.model.Address;
import com.devcamp.person.model.Professor;
import com.devcamp.person.model.Student;
import com.devcamp.person.model.Subject;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;

@RestController
public class PersonController {
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Student> getListStudent() {
        ArrayList<Student> listStudent = new ArrayList<Student>();

        Subject subjectHoaDaiCuong = new Subject("Hóa đại cương", 1,
                                                    new Professor(50, "male", "PhucDM", new Address()));
        ArrayList<Subject> subjects01 = new ArrayList<Subject>();
        subjects01.add(subjectHoaDaiCuong);
        Student studentQ = new Student(27, "male", "Quan", new Address(), 1, subjects01);
        Student studentB = new Student(23, "female", "Boi", new Address(), 2,
                new ArrayList<Subject>() {
                    {
                        add(new Subject("Vậy lý đại cương", 1,
                                new Professor(54, "male", "HoBa", new Address())));
                        add(new Subject("Toán cao cấp", 1,
                                new Professor(52, "female", "DanhNguyen", new Address())));
                    }
        });
        Student studentT = new Student(20, "female", "Thu", new Address(), 3,
                new ArrayList<Subject>(Arrays.asList(
                        new Subject("Anh văn", 1,
                                new Professor(53, "male", "HoangNg", new Address())),
                        new Subject("Sinh học", 1,
                                new Professor(45, "female", "ThuyD", new Address())))));
        listStudent.add(studentQ);
        listStudent.add(studentB);
        listStudent.add(studentT);
        return listStudent;
    }
}
